<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FromController extends Controller
{
    public function index(Request $request)
    {

        $FirstName = $request->fn;
        $LastName = $request->ls;
        // dd($FirstName);
        return view('selamat', ['firstname' => $FirstName, 'lastname' => $LastName]);
    }
}
