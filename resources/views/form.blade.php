<!DOCTYPE html>
<html>

<head>
    <title>Biografi</title>
</head>

<body>
    <fieldset>
        <h1>Buat Account Baru!</h1>
        <h4>Sign Up From</h4>

        <form action="/form" method="POST">
            @csrf
            <label for="">First Name:</label>
            <br><br>
            <input name="fn" type="text">
            <br><br>
            <label for="">Last name:</label>
            <br><br>
            <input name="ls" type="text">
            <br><br>
            <label for="">Gender:</label> <br><br>
            <input type="radio" name="gender" value="0"> Male <br>
            <input type="radio" name="gender" value="1"> Female <br>
            <input type="radio" name="gender" value="2"> Other <br><br>
            <label for="">Nationaly:</label><br><br>
            <select>
                <option value="id">Indonesia</option>
                <option value="si">Singapore</option>
                <option value="ma">Malaysia</option>
                <option value="ka">Kamboja</option>
            </select>
            <br><br>
            <label for="">Languange Spoken:</label><br><br>
            <input type="checkbox" name="Languange" value="0"> Bahasa Indonesia <br>
            <input type="checkbox" name="Languange" value="1"> English <br>
            <input type="checkbox" name="Languange" value="2"> Other <br>
            <br>
            <label for="">Bio:</label> <br><br>
            <textarea name="" id="" cols="30" rows="10"></textarea>

            <button type="submit">Sign UP</button>
        </form>
        <br>         
    </fieldset>

    <!-- input melalui from action 
        <form action="selamat.html">
        <input type="submit" onclick="selamat.html" value="Sign up">
    </form> 
-->

</body>

</html>